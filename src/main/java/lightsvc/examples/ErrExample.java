package lightsvc.examples;

import static lightsvc.err.Err.Translation.from;

import lightsvc.err.Err;
import lightsvc.err.Exceptions;

public class ErrExample {

  public static enum ErrRetrieveAccount {
    accountId_mustBeFound,
    storage_mustBeAvailable,
  }

  public static void retrieveAccount(String accountId) {
    //
    // Using conventional translation
    var account1 = Exceptions.translate(
      ()->selectAccount(accountId),
      from(ErrSelectAccount.accountId_mustBeFound).to(e->new Err(ErrRetrieveAccount.accountId_mustBeFound)),
      from(
        ErrSelectAccount.database_mustNotReturnNetworkErrors,
        ErrSelectAccount.database_mustNotReturnServiceErrors
      )
        .to(e->new Err(ErrRetrieveAccount.storage_mustBeAvailable)),
      from(ErrSelectAccount.database_mustNotReturnServiceErrors)
        .to(e->new Err(ErrRetrieveAccount.storage_mustBeAvailable))
    );
    //
    // Using direct translation.
    var account2 = Exceptions.translate(
      ()->selectAccount(accountId),
      from(ErrExample.ErrSelectAccount.accountId_mustBeFound).to(ErrRetrieveAccount.accountId_mustBeFound),
      from(
        ErrSelectAccount.database_mustNotReturnNetworkErrors,
        ErrSelectAccount.database_mustNotReturnServiceErrors
      )
        .to(ErrRetrieveAccount.storage_mustBeAvailable)
    );
  }

  public static enum ErrSelectAccount {
    accountId_mustBeFound,
    database_mustNotReturnNetworkErrors,
    database_mustNotReturnServiceErrors,
  }

  private static String selectAccount(String accountId) {
    throw new RuntimeException("accountId must be found.");
  }

}
