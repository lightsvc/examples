package lightsvc.examples;

import static lightsvc.err.Exceptions.any;
import static lightsvc.err.Exceptions.hasMsg;
import static lightsvc.err.Exceptions.Translation.when;

import lightsvc.err.Err;
import lightsvc.err.Exceptions;

public class ExceptionsExample {

  public static void main(String[] args) {
    var accountId = "00973596457";
    
    var account1 = Exceptions.translate(
      ()->selectAccount(accountId),
      when(hasMsg("accountId must be found.")).to(e->new IllegalArgumentException(e)),
      when(any()).to(e->new RuntimeException("A general exception has ocurred.", e))
    );
    
    String account2;
    try {
      account2 = selectAccount(accountId);
    } catch (Exception ex) {
      throw Exceptions.translate(
        ex,
        when(hasMsg("accountId must be found.")).to(e->new IllegalArgumentException(e)),
        when(any()).to(e->new RuntimeException("A general exception has ocurred.", e))
      );
    }
  }
  
  private static String selectAccount(String accountId) {
    throw new RuntimeException("accountId must be found.");
  }
}
